<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// public routes
Route::post('login', 'API\AuthController@login')->name('login.api');
Route::post('register', 'API\AuthController@register')->name('register.api');

// private routes
Route::middleware('auth:api')->group(function(){

    Route::get('logout', 'API\AuthController@logout')->name('logout.api');
    Route::resource('users', 'API\UsersController');

});

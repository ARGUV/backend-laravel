## This is Backend Project

## Domain

Use for this project - http://127.0.0.1:8001 domain

## Registration

/api/register

````
name
email
password
password_confirmation
````

## Login

/api/login

````
email
password
````

## Logout

/api/logout

````
token
````

## Get All Users

/api/users

````
token
````

## Get User By Id

/api/users/{id}

````
token
````
